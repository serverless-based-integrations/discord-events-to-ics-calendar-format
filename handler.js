'use strict';

const ical = require('ical-generator');
const { DateTime } = require("luxon");


function isNullOrUndefined(item) {
  return item == undefined || item == null;
}

const getEvents = async (event) => {
  const DISCORD_API_BASE_URL = "https://discord.com/api/v10"; 
  let eventListUrl = DISCORD_API_BASE_URL+`/guilds/${process.env.GUILD_ID}/scheduled-events`
  console.log(`Contacting:\n\t${eventListUrl}`)
  const response = await fetch(eventListUrl, {
    method: "GET",
    headers: {
      Authorization: `Bot ${process.env.BOT_TOKEN}`,
      "Content-Type": "application/json",
    }
  }).then(response => {
    let data = response.json();
    return data;
  })
  return response;
}

const returnCalendar = async (functionParams) => {
  const calendar = new ical.ICalCalendar({
    name: process.env.CALENDAR_NAME,
    timezone: 'UTC'
  });

  let eventList = await getEvents();
  eventList.forEach(event => {
    console.log("\nEvent from discord:")
    console.log(event);
    console.log("\n\n")
    let location = isNullOrUndefined(event.location) ? "" : event.location;
      let description = isNullOrUndefined(event.description) ? "" : event.description;
      let url = isNullOrUndefined(event.url) ? "" : event.url;
      let organizerName = isNullOrUndefined(event["organizer-name"])? "N/A" : event["organizer-name"];
      let organizerEmail = isNullOrUndefined(event["organizer-email"])? "N/A" : event["organizer-email"];
      let organizer = {};
      if (!isNullOrUndefined(organizerEmail) && !isNullOrUndefined(organizerName)) {
        organizer["name"] = organizerName;
        organizer["email"] = organizerEmail;
      }

      let generatedStartDate = event.scheduled_start_time;
      let generatedEndDate = event.scheduled_end_time;
      
      const eventPayload = {
        start: generatedStartDate,
        end: generatedEndDate,
        summary: event.name,
        description: description,
        location: location,
        url: url,
        organizer,
      }

      console.log(`\n[i]\tGenerated Event payload:`);
      console.log(eventPayload);
      calendar.createEvent(eventPayload);
  })

  const icsFile = calendar.toString();

  // Return the ICS calendar file as a download
  return {
    statusCode: 200,
    headers: {
      'Content-Type': 'text/calendar',
      'Content-Disposition': 'attachment; filename=calendar.ics'
    },
    body: icsFile
  };
        
}

module.exports.getEvents = getEvents;
module.exports.returnCalendar = returnCalendar;